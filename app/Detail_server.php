<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail_server extends Model
{
  protected $fillable = [
    'id', 'vmid'
  ];
  
  protected $table = 'detailserver';
}
