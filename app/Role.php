<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  protected $fillable = ['name'];

  // Relation
  public function users(){
    return $this->hasMany('App\User');
  }
}
