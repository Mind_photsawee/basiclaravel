<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class CustomResetPasswordNotification extends Notification
{
  use Queueable;
  public $email, $token;

  /**
   * Create a new notification instance.
   *
   * @return void
   */
  public function __construct($email, $token)
  {
    $this->email = $email;
    $this->token = $token;
  }

  /**
   * Get the notification's delivery channels.
   *
   * @param  mixed  $notifiable
   * @return array
   */
  public function via($notifiable)
  {
    return ['mail'];
  }

  /**
   * Get the mail representation of the notification.
   *
   * @param  mixed  $notifiable
   * @return \Illuminate\Notifications\Messages\MailMessage
   */
  public function toMail($notifiable)
  {
    $reset_link = config('app.url').route('password.reset', $this->token, false)."?email=".$this->email;
    return (new MailMessage)
      ->subject(Lang::getFromJson('Reset Password Notification'))
      ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
      ->action(Lang::getFromJson('Reset Password'), url($reset_link))
      ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));
  }
}