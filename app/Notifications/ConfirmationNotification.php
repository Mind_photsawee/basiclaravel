<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ConfirmationNotification extends Notification
{
  public $user;

  public function __construct($user)
  {
    $this->user = $user;
  }

  public function via($notifiable)
  {
    return ['mail'];
  }

  public function toMail($notifiable)
  {
    return (new MailMessage)
      ->subject('Confirmation instructions')
      ->greeting('Hello! '. $this->user->fullname)
      ->line('Thanks so much for joining. To finish signing up, 
          you just need to confirm that we got your email right.')
      ->action('Confirm Your Email', url('register/verify', $this->user->confirmation_code));
  }
}
