<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size_server extends Model
{
  protected $fillable = [
    'memory', 'cpu', 'ssd', 'bandwidth' 
  ];

  protected $table = 'size_server';
}
