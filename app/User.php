<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\CustomResetPasswordNotification;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password', 'first_name', 'last_name', 'phone_number',
    'address', 'address_line2', 'city', 'zip_code', 'country', 'company', 'role_name'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  // Overide
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new CustomResetPasswordNotification($this->email, $token));
  }

  // Relation
  public function roles(){
    return $this->belongsTo('App\Role');
  }
  public function servers(){
    return $this->hasMany('App\Server');
  }
  public function dnses() {
    return $this->hasMany('App\DNS');
  }
  public function sshkeys() {
    return $this->hasMany('App\SSHKEY');
  }
}
