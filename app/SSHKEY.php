<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SSHKEY extends Model
{
  protected $fillable = ['name', 'ssh_key'];
  protected $table = 'sshkey';
}
