<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ConfirmationNotification;

class HomeController extends Controller
{
  /**
    * Create a new controller instance.
    *
    * @return void
    */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
  public function index()
  {
    return view('auth.login');
  }
}
