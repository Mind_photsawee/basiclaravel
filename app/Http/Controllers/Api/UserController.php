<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ConfirmationNotification;

class UserController extends Controller
{
  public function confirm($confirmation_code)
  {
    if(!$confirmation_code)
    {
      return redirect("login")
        ->with('msg_error',"Confirmation code is incorrect. 
            Please request to re-send confirmation email agian"); 
    }

    $user = User::whereConfirmationCode($confirmation_code)->first();
  
    if (!$user)
    {
      return redirect("login")
        ->with('msg_error',"Link has already been used or you're confirmed."); 
    }
  
    $user->confirmed = 1;
    $user->confirmation_code = null;
    $user->save();

    return redirect("login")
      ->with('msg_success',"You're confirmed. Please login for using website.");
  }
  public function resendEmail($email){
    $id = User::where('email','LIKE','%'.$email.'%')->first()->id;
    $user = User::findOrFail($id);
    $user->confirmation_code = str_random(30);
    $user->save();
    $user->notify(new ConfirmationNotification($user));
    
    return response()->json('Confirmaion email sent. Please check your email.');
  }
  public function checkConfirmed($email){
    $assure = User::where('email','LIKE','%'.$email.'%')->first()->confirmed;
    
    return response()->json($assure);
  }
  public function showProfile($email){
    $id = User::where('email','LIKE','%'.$email.'%')->first()->id;
    $user = User::findOrFail($id);
    
    return response()->json($user);
  }
  public function update($email, Request $request){
    $id = User::where('email','LIKE','%'.$email.'%')->first()->id;
    $new_profile = User::findOrFail($id);
    $new_profile->update($request->all());

    return response()->json($new_profile);
  }
}
