<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;

class LoginController extends Controller
{
  public $success_status = 200;

  public function login(Request $request) { 
    $credentials = $request->only('email', 'password');
    if (Auth::attempt($credentials)) {
      $user = Auth::user();
      $responce_msg = array(
        'status' => 'success',
        'access_token' => $user->createToken(env('TOKEN_NAME'))->accessToken,
        'message' => 'login success'
      );
      return response()->json($responce_msg, $this->success_status); 
    } 
    else{
      $responce_msg = array(
        'status' => 'fail',
        'access_token' => null,
        'message' => 'email or password is incorrect.',
      );
      return response()->json($responce_msg, 500); 
    }
  }
}
