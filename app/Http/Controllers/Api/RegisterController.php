<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;

class RegisterController extends Controller
{
  public $success_status = 200;
  
  public function register(RegisterRequest $request){
    $input = $request->all(); 
    $input['password'] = bcrypt($input['password']);
    $user = User::create($input); 
    $responce_msg = array(
      'status' => 'success',
      'email' => $user->email,
      'access_token' => $user->createToken(env('TOKEN_NAME'))->accessToken,
    );
    return response()->json($responce_msg, $this->success_status);
  }
}
