<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function index()
  {
    return view('index');
  }
  /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function create()
  {

  }

  /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request)
  {
    
  }

  /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function showUser($email)
  {
    $user = User::where('email','NOT LIKE','%'.$email.'%')->get();
    
    return response()->json($user);
  }

  public function server()
  {
    $user = User::all();
    
    return response()->json($user);
  }

  public function test()
  {
    $test = User::all();
    
    return response()->json($test);
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function showData($id){
    $data = array(
      'user' => User::findOrFail($id),
      'role' => Role::all()
    );

    return response()->json($data);
  }
  public function edit()
  {
    return view('edituser');
  }

  /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update(Request $request, $id)
  {
    $new_profile = User::findOrFail($id);
    $new_profile->update($request->all());

    return response()->json($new_profile);
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy($id)
  {

  }
}
