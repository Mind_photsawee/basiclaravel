<?php
namespace App\Http\Controllers\Admin;

use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Cookie;

class RoleController extends Controller
{
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function index()
  {
    $roles = Role::all();
    $data = array(
      'roles' => $roles
    );

    return view('roles.index',$data);
  }
  /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
  public function create()
  {
    return view('roles.index');
  }

  /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
  public function store(Request $request)
  {
    $request->validate([
      'name' => 'required'
    ]);
    $role = new Role();
    $role->name = $request->name;
    $role->save();

    return redirect('role')->with('message', 'User role created successfully');
  }

  /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function show()
  {
    //
  }

  /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */

  public function edit($id)
  {
    if($id !== '') {
      $roles = Role::find($id);
      $editrole = Role::all();
      $data = array(
        'roles' => $roles,
        'editrole' => $editrole
      );

      return view('roles.index',$data);
    }
  }

  /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function update(Request $request, $id)
  {
    $request->validate([
      'name' => 'required'
    ]);
    $roles = Role::find($id);
    $roles->name = $request->name;
    $roles->save();
    Session::flash('message','Success update user role!!');

    return redirect('role');
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
  public function destroy($id)
  {
    $role = Role::find($id);
    $role->delete();
    Session::flash('message', 'Success Delete user role!!');
    
    return redirect('role');
  }
}
