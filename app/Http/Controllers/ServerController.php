<?php

namespace App\Http\Controllers;

use App\Size_server;
use App\Server;
use App\Detail_server;
use App\User;
use App\DNS;
use App\SSHKEY;

use Auth;
// use Illuminate\Support\Facades\Auth; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ProxmoxVE\Proxmox;

class ServerController extends Controller
{
  /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

  protected $proxmoxAPI;
  protected $allNodes;
  protected $ostemplates;

  public function __construct()
  {

    $credentials = [
    'hostname' => env('PROXMOX_URL'),
    'username' => env('PROXMOX_USERNAME'),
    'password' => env('PROXMOX_PASSWORD'),
    ];

    // Then simply pass your credentials when creating the API client object.
    $this->proxmoxAPI = new Proxmox($credentials);
    $this->allNodes = $this->proxmoxAPI->get('/nodes/pve06');
    $this->ostemplates = array(
      'centos6' => 'local:vztmpl/centos-6-default_20161207_amd64.tar.xz',
      'centos7' => 'local:vztmpl/centos-7-default_20171212_amd64.tar.xz'
    );
  }

  public function index()
  { 
    return view('server');  
  }

  public function genVmid()
  {
    $detail = Server::all()->last();

    if(empty($detail['id'])){
      $detail['vmid'] = env('VM_ID_START') ?: 500;
    }
    else{
      $detail['vmid'] = $detail['vmid'] + 1;
    }
    
    return response()->json($detail['vmid']);
  }

  public function sizeServer()
  {
    $node_name = env('PROXMOX_NODE');
    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->get("/nodes/".$node_name."/qemu/100/snapshot"));
      $server = Size_server::all();
      $sizeServer = array(
        'server' => $server,
        'proxmox' => $proxmox
      );   
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }   

    return response()->json($sizeServer);
  }

  public function createStorage($vmid) {
    $node_name = env('PROXMOX_NODE');
    $storage_name = "local-test";

    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/qemu/".$vmid."/agent/set-user-password", [
        'username' => "root",
        'password' => '1234567',
        'crypted' => 1,
      ]));
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
    return response()->json($proxmox);
  }

  public function createServer(Request $request)
  {
    $user = User::where('email', $request->user)->first();
    $data = $request->all();
    if($user) {
      $node_name = env('PROXMOX_NODE');
      $ip_duplicate = true;
      $ipaddress = '192.168.1.';
      do {  
        $prefix = rand(15,255);
        if(Server::where('ip', '=', $ipaddress.$prefix)->count() == 0) {
          $ipaddress = $ipaddress.$prefix;
          $data['ip'] = $ipaddress;
          $ip_duplicate = false;
        }
      } while($ip_duplicate);
      $params = array(
        'ostemplate' => $this->ostemplates[$request->os],
        'vmid' => $request->vmid,
        'cores' => $request->cores,
        'hostname' => $request->hostname,
        'net0' => 'bridge=vmbr0,name=eth0,ip='.$ipaddress.'/24,gw=192.168.1.1',
        'memory' => $request->memory,
        'swap' => $request->memory,
        'password' => $request->root_password ?: "1234567",
        'rootfs' => $request->rootfs,
        'searchdomain' => '8.8.8.8',
        'nameserver' => '8.8.4.4',
      );
      if($this->proxmoxAPI->login()){
        $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc", $params));
        if($proxmox[0]["data"] != null) {
          $user->servers()->create($data);

          return response()->json("created");
        }
        else {
          return response()->json($proxmox[0]["errors"]);
        }
        /*if($createvm[0]["data"] != null) {
          $storage_name = "local-test";
          $create_storage = array($this->proxmoxAPI->create("/nodes/".$node_name."/storage/".$storage_name."/content", [
            'filename' => "vm-".$vmid."-disk-1.raw",
            'size' => '40G',
            'vmid' => $vmid,
            'format' => 'raw'
          ]));
          if($create_storage[0]["data"] != null) {
            $path = "local-test:".$vmid."/vm-".$vmid."-disk-1.raw,cache=writeback";
            $proxmox = array($this->proxmoxAPI->set("/nodes/".$node_name."/qemu/".$vmid."/config", [
              'virtio0' => $path,
              'bootdisk' => 'virtio0',
              'citype' => 'nocloud',
              'ciuser' => 'root',
              'cipassword' => '1234567',
              'ipconfig0' => 'gw=192.168.1.1,ip=192.168.1.15/24'
            ]));
          }
        }*/
      }
      else{
        print("Login to Proxmox Host failed.\n");
      }

      // return response()->json($proxmox[0]["errors"]);
    }
  }

  public function recreateServer($vmid)
  {
    $server = Server::where('vmid', '=', $vmid)->first();
    if($server) {
      $node_name = env('PROXMOX_NODE');
      $params = array(
        'ostemplate' => $this->ostemplates[$server->os],
        'vmid' => $server->vmid,
        'cores' => $server->cores,
        'hostname' => $server->hostname,
        'net0' => 'bridge=vmbr0,name=eth0,ip='.$server->ip.'/24,gw=192.168.1.1',
        'memory' => $server->memory,
        'swap' => $server->memory,
        'password' => $server->root_password ?: "1234567",
        'rootfs' => $server->rootfs,
        'searchdomain' => '8.8.8.8',
        'nameserver' => '8.8.4.4',
      );
      if($this->proxmoxAPI->login()){
        $proxmox_delete = array($this->proxmoxAPI->delete("/nodes/".$node_name."/lxc/".$vmid));
        sleep(3);
        $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc", $params));
      }
      else{
        print("Login to Proxmox Host failed.\n");
      }

      return response()->json("created");
    }
  }
  
  public function FK_server($email)
  {
    $id = User::where('email','LIKE','%'.$email.'%')->first()->id;
    $server = Server::all()->last();
    if(!empty($server) && empty($server->user_id)){
      $server->user_id = $id;
      $server->save();
    }
  }

  public function startServer($vmid)
  {
    // $startServer = 'start '.$vmid;
    // return response()->json($startServer);

    $node_name = env('PROXMOX_NODE');
    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc/".$vmid."/status/start"));
      return response()->json("started");
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
  }

  public function stopServer($vmid)
  {
    // $stopServer = 'stop '.$vmid;
    // return response()->json($stopServer);

    $node_name = env('PROXMOX_NODE');
    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc/".$vmid."/status/stop"));
      return response()->json("stopped");
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
  }

  public function reinstallServer($vmid)
  {
    // $reinstallServer = 'reinstall '.$vmid;
    // return response()->json($reinstallServer);
    $node_name = env('PROXMOX_NODE');
    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc/".$vmid."/status/stop"));
      return response()->json("reseted");
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
  }

  public function consoleServer($vmid)
  {
    // $reinstallServer = 'reinstall '.$vmid;
    // return response()->json($reinstallServer);
    $node_name = env('PROXMOX_NODE');
    if($this->proxmoxAPI->login()){
      $proxmox = array($this->proxmoxAPI->create("/nodes/".$node_name."/lxc/".$vmid."/vncproxy"));
      return response()->json(url('openConsole/'.$vmid));
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
  }

  public function openConsole($vmid) {
    $url = "https://".env('PROXMOX_URL').":8006/?console=lxc&novnc=1&vmid=".$vmid."&node=".env('PROXMOX_NODE');
    return view('novnc')->with([
      'url' => $url
    ]);
  }

  public function destroyServer($vmid)
  {
    // $destroyServer = 'destroy '.$vmid;
    // return response()->json($destroyServer);
    $node_name = env('PROXMOX_NODE');
    // $server = $vmid->all();
    // Server::delete($server);
    if($this->proxmoxAPI->login()){
      Server::where('vmid', $vmid)->delete();
      $proxmox = array($this->proxmoxAPI->delete("/nodes/".$node_name."/lxc/".$vmid));
      return response()->json("deleted");
    }
    else{
      print("Login to Proxmox Host failed.\n");
    }
  }

  public function createDns() {
    return view('addDns');
  }

  public function editDns($id) {
    $dns = DNS::find($id);
    return view('addDns')->with(['dns' => $dns]);
  }

  public function updateDns($id, Request $request) {
    $dns = DNS::find($id);
    if($dns) {
      $dns->fill($request->all());
      $dns->save();
      return response()->json("updated");
    }
  }

  public function destroyDns($id) {
    $dns = DNS::find($id);
    if($dns) {
      $dns->delete();
      return response()->json("deleted");
    }
  }

  public function addDNS(Request $request) {
    $user = User::where('email', '=', $request->email)->first();
    if($user) {
      $user->dnses()->create($request->all());
      return response()->json("created");
    }
  }

  public function getDNS($email) {
    $user = User::where('email', '=', $email)->first();
    if($user) {
      return response()->json($user->dnses()->get());
    }
  }

  public function createSsh() {
    return view('addSSH');
  }

  public function editSsh($id) {
    $sshkey = SSHKEY::find($id);
    return view('addSSH')->with(['sshkey' => $sshkey]);
  }

  public function updateSsh($id, Request $request) {
    $sshkey = SSHKEY::find($id);
    if($sshkey) {
      $sshkey->fill($request->all());
      $sshkey->save();
      return response()->json("updated");
    }
  }

  public function destroySsh($id) {
    $sshkey = SSHKEY::find($id);
    if($sshkey) {
      $sshkey->delete();
      return response()->json("deleted");
    }
  }

  public function addSsh(Request $request) {
    $user = User::where('email', '=', $request->email)->first();
    if($user) {
      $user->sshkeys()->create($request->all());
      return response()->json("created");
    }
  }

  public function getSsh($email) {
    $user = User::where('email', '=', $email)->first();
    if($user) {
      return response()->json($user->sshkeys()->get());
    }
  }

  public function createSSD() {
    return view('addSSH');
  }
}
