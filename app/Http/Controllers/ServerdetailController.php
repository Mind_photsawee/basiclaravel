<?php

namespace App\Http\Controllers;

use App\Size_server;
use App\Server;
use App\Detail_server;
use App\User;

use Auth;
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use ProxmoxVE\Proxmox;
use Carbon\Carbon;

class ServerdetailController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */

  protected $proxmoxApi;
  protected $allNodes;

  public function __construct()
  {
    $credentials = [
    'hostname' => env('PROXMOX_URL'),
    'username' => env('PROXMOX_USERNAME'),
    'password' => env('PROXMOX_PASSWORD'),
    ];

    // Then simply pass your credentials when creating the API client object.
    $this->proxmoxApi = new Proxmox($credentials);
    $this->allNodes = $this->proxmoxApi->get('/nodes/pve06');
  }

    public function instances($email)
  {
    $id = User::where('email','LIKE','%'.$email.'%')->first()->id;
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $instances = array( 
        'proxmox' => array($this->proxmoxApi->get("/nodes/".$node_name."/lxc")),
        'user_use' => Server::where('user_id','LIKE','%'.$id.'%')->orderBy('vmid', 'desc')->get(),
      );
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    return response()->json($instances);
  }
  public function overview($vmid)
  {
    //$proxmox = Server::where('vmid','LIKE','%'.$vmid.'%')->get();
    $server = Server::where('vmid', '=', $vmid)->first();
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $proxmox = $this->proxmoxApi->get("/nodes/".$node_name."/lxc/".$vmid."/status/current");
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    $response = array(
      'server_detail' => $server,
      'server_info' => $proxmox
    );
    return response()->json($response);
  }

  public function usageMonitors($vmid) {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $results = $this->proxmoxApi->get("/nodes/".$node_name."/lxc/".$vmid."/rrddata",[
        'timeframe' => 'hour',
        'cf' => 'AVERAGE'
      ]);
      $cpu_info = [];
      $memory_info = [];
      $bandwidth_info = [];
      $disk_info = [];
      foreach ($results["data"] as $result) {
        if(count($result) > 1) {
          $datetime = Carbon::createFromTimestamp(
            $result["time"], 'Asia/Bangkok')->format('Y-m-d h:i');
          if(isset($result["cpu"])) {
            $cpu_info[$datetime][] = number_format($result["cpu"] * 100, 2);
          }

          if(isset($result["mem"])) {
            $memory_info[$datetime][] = number_format((floatval($result["mem"]) / 1048576), 2);
          }

          if(isset($result["netin"]) && isset($result["netout"])) {
            $bandwidth_info[$datetime][] = array(
              "netin" => $result["netin"],
              "netout" => $result["netout"]
            );
          }

          if(isset($result["diskwrite"]) && isset($result["diskread"])) {
            $disk_info[$datetime][] = array(
              "diskwrite" => number_format(floatval($result["diskwrite"]) / 1048576, 2),
              "diskread" => number_format(floatval($result["diskread"]) / 1048576, 2)
            );
          }
        }
      }
      $proxmox = array(
        'cpu_info' => array_slice($cpu_info, -18, 18, true),
        'memory_info'=> array_slice($memory_info, -18, 18, true),
        'bandwidth_info' => array_slice($bandwidth_info, -18, 18, true),
        'disk_info' => array_slice($disk_info, -18, 18, true)
      );
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    return response()->json($proxmox);
  }

  public function snapshot($vmid)
  {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $snapshots = array($this->proxmoxApi->get("/nodes/".$node_name."/lxc/".$vmid."/snapshot"));
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    return response()->json($snapshots);
  }

  public function changeInfomation($vmid, Request $request) {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $proxmox = array($this->proxmoxApi->set("/nodes/".$node_name."/lxc/".$vmid."/config", [
        'hostname' => $request->hostname
      ]));
      $server = Server::where('vmid', '=', $vmid)->first();
      if($server) {
        $server->fill($request->all());
        $server->save();
      }
      return response()->json("changed");
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
  }

  public function changeOS($vmid, Request $request) {
    $server = Server::where('vmid', '=', $vmid)->first();
    $node_name = env('PROXMOX_NODE');
    if($server) {
      if($this->proxmoxApi->login()){
        $proxmox = array($this->proxmoxApi->create("/nodes/".$node_name."/lxc/".$vmid."/status/stop"));
        $server->os = $request->os["name"];
        $server->save();
        return response()->json("changed");
      }
      else{
        print("Login to Proxmox Host failed.\n");
      }
    }
  }

  public function dns($vmid)
  {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $senddns = array($this->proxmoxApi->get("/nodes/".$node_name."/lxc/".$vmid."/config"));
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    //dd($senddns);
    return response()->json($senddns);
  }

  public function IPv4()
  {
    $ip4 = User::all();
    
    return response()->json($ip4);
  }

  public function createSnapshot($vmid, Request $request) {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $proxmox = array($this->proxmoxApi->create("/nodes/".$node_name."/lxc/".$vmid."/snapshot/",[
        'snapname' => $request->snapname ?: str_random(15)
      ]));
    }

    return response()->json("snapshot created");
  } 

  public function deleteSnapshot($name, $vmid)
  {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $proxmox = array($this->proxmoxApi->delete("/nodes/".$node_name."/lxc/".$vmid."/snapshot/".$name));
    }

    return response()->json($proxmox);
  }

  public function editDns($vmid)
  {
    $node_name = env('PROXMOX_NODE');
    if ($this->proxmoxApi->login()) {
      $edit = array($this->proxmoxApi->create("/nodes/".$node_name."/lxc/".$vmid."/config"));
    } else {
      print("Login to Proxmox Host failed.\n");
      exit;
    }
    dd($edit);
    return response()->json($edit);
  }

  public function index()
  {
    return view('information');
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param int $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param int $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }

  /**
  * Update the specified resource in storage.
  *
  * @param \Illuminate\Http\Request $request
  * @param int $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param int $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}
