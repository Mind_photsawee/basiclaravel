<?php

namespace App\Http\Controllers\basicLaravel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BasicController extends Controller
{
    public function main () {
      /*
        fuction นี้จะทำการเรียก file main ที่อยู่ใน folder basicLaravel

        view() เป็น fuction ตายตัว เอาไว้ใช้เรียก file ในนั้นออกมาแสดงบนหน้าจอ

        ทุกอย่างที่อยู่หน้า .main จะเป็น folder เสมอ เช่น
         2.3.main หมายความว่า file main อยู่ใน folder 3 แล้ว folder 3 อยู่ใน folder 2 อีกที เป็นต้น
        คำว่า main อ่ะ เป็นชื่อที่เราตั้งเองนะ
      */
      return view('basicLaravel.main');
    }

    public function passingData ($parameter) {
      /*
        มีการรับค่ามา 1 ค่า เข้ามาแล้วส่งไปแสดงหน้าจอ
      */
      return view('basicLaravel.main')
              ->with('parameter', $parameter);
    }

    public function passingData2 ($parameter, $parameter2) {
      /*
        มีการรับค่ามา 2 ค่า เข้ามาแล้วส่งไปแสดงหน้าจอ
      */
      return view('basicLaravel.main')
              ->with('parameter', $parameter)
              ->with('parameter2', $parameter2);
    }

    public function passingData3 ($parameter, $parameter2, $parameter3) {
      /*
        มีการรับค่ามา 3 ค่า เข้ามาแล้วส่งไปแสดงหน้าจอ
      */
      return view('basicLaravel.main')
              ->with('parameter', $parameter)
              ->with('parameter2', $parameter2)
              ->with('parameter3', $parameter3);
    }

    public function passingParam ($passing=null) {
      return view('basicLaravel.main2')
              ->with('passing', $passing);
    }
    
}
