<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
  public function show(){
    return view('profile');
  }
  public function order(){
    return view('orderServer');
  }
  public function information(){
    return view('information');
  }
  public function deleteSession(Request $request){
    $request->session()->flush();
  }
}
