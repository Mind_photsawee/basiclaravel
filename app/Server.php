<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
  protected $fillable = [
    'vmid', 'rootfs', 'cores', 'bwlimit', 'memory', 'hostname', 'user_id',
    'ip', 'root_password', 'dns_id', 'sshkey_id', 'os'
  ];
  
  protected $table = 'server';

  // Relation
  public function users(){
    return $this->belongsTo('App\User');
  }
}
