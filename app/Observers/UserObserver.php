<?php

namespace App\Observers;

use Mail;
use App\User;
use App\Notifications\ConfirmationNotification;

class UserObserver
{
  public function creating(User $user)
  {
    $user->confirmation_code = str_random(30);
  }

  public function created(User $user)
  {
    $user->notify(new ConfirmationNotification($user));
  }
}
