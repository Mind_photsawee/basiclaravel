<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DNS extends Model
{
  protected $fillable = ['domain', 'ipaddress'];
  protected $table = 'dns';
}
