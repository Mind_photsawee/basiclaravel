@extends('layouts.app')

@section('custom_style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/home.css') }}">
@endsection

@section('content')
  <meta charset="utf-8" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container" id="home-page">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light float-right">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-element" aria-controls="navbar-element" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar-element">
            <div class="navbar-nav">
              <li>
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
              </li>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Dashboard</div>
          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif
            You are logged in! 
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
