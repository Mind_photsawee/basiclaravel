@extends('layouts.app')

@section('custom_style')
  <!-- Bootstrap Core CSS -->
  <link href="/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
  <!-- Custom CSS -->
	<link href="/css/helper.css" rel="stylesheet">
  <link href="/css/style.css" rel="stylesheet">
@endsection

@section('content')
	<meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
      <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
      </svg>
    </div>
    <div id="sidebarheader-section"><form id="logout-form"></form>
      <sidebarheader></sidebarheader>
      <div id="main-wrapper">
      <!-- Header -->
        <!-- Body page wrapper  -->
        <div class="page-wrapper">
          <div class="container-fluid">
            <!-- Start Page Content -->
            <div class="row">
              <!-- Alert Assure -->
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">
                    <div class="card-two">
                      <div class="container" id="role-index">
                        <div class="row" id="role">
                          <div id="role-section" class="col-md-12">
                            <div class="row">
                              <div class="col-sm-4 col-md-4">
                                @if(isset($roles->id))
                                  {{ Form::open(['method' => 'put', 'route' => ['role.update', $roles->id] ]) }}
                                @else
                                  {{ Form::open(['url' => 'role']) }}
                                @endif
                                <h5 class="text-dark">User admin</h5>
                                @if($message = session()->pull('message'))
                                  <div class="alert alert-info">{{ $message }}</div>
                                @endif
                                @if(count($errors))
                                  <ul class="alert alert-danger well">
                                    @foreach($errors->all() as $error)
                                      <li class="list-unstyled">{{ $error }}</li>
                                    @endforeach
                                  </ul>
                                @endif
                                <div class="form-group">
                                  <p class="text-left text-secondary"></p>
                                </div>
                                  {{ csrf_field() }}
                                  @if(isset($roles->name))
                                    {{ Form::text('name',$roles->name,['class' => 'form-control']) }}
                                  @else
                                    <input class="form-control" type="text" id="name" name="name" placeholder="username">
                                  @endif
                                  <br>
                                  {{ Form::submit('Submit',['class' => 'btn btn-info']) }}
                                {{ Form::close() }}
                              </div>
                              <!-- End index -->
                              <div class="col-sm-4 col-md-8">
                                <!-- Start Form -->
                                <div class="container">
                                  @if(isset($roles->id))
                                    <table class="table table-bordered table-hover">
                                      <thead>
                                        <tr>
                                          <th width="400" height="40">User</th>
                                          <th width="90" height="40">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($editrole as $l)
                                          <tr>
                                            <td>{{ $l['name'] }}</td>
                                            <td>
                                              {{ Form::open(['route' => ['role.destroy',$l['id'], 'method' => "DELETE"] ]) }}
                                              <input type="hidden" name="_method" value="delete" />
                                              {{ Html::link('role/'.$l['id'].'/edit', '', array('class'=> 'fa fa-pencil btn btn-warning')) }}
                                              <button type="submit" class="fa fa-trash-o btn btn-danger" name="submit" onclick="return confirm('Are you sure?')"></button>
                                              {{ Form::close() }}
                                            </td>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  @else         
                                    <table class="table table-bordered table-hover">
                                      <thead>
                                        <tr>
                                          <th width="400" height="40">User</th>
                                          <th width="90" height="40">Action</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($roles as $l)
                                          <tr>
                                            <td>{{ $l['name'] }}</td>
                                            <td>
                                              {{ Form::open(['route' => ['role.destroy',$l['id'], 'method' => "DELETE"] ]) }}
                                              <input type="hidden" name="_method" value="delete" />
                                              {{ Html::link('role/'.$l['id'].'/edit', '', array('class'=> 'fa fa-pencil btn btn-warning')) }}
                                              <button type="submit" class="fa fa-trash-o btn btn-danger" name="submit" onclick="return confirm('Are you sure?')"></button>
                                              {{ Form::close() }}
                                            </td>
                                          </tr>
                                        @endforeach
                                      </tbody>
                                    </table>
                                  @endif 
                                </div>
                                <!-- End Form -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Column -->
            </div>
            <!-- End PAge Content -->
          </div>
          <!-- End Container fluid  -->
          <!-- footer -->
          <footer class="footer"> © 2018 All rights reserved. Template designed by <a href="https://colorlib.com">Colorlib</a></footer>
          <!-- End footer -->
        </div>
        <!-- End Body page wrapper  -->
      </div>
    </div>
  </div>
@endsection

@section('custom_script')
  <script type="text/javascript" src="{{ asset('/js/sidebarheader.js') }}"></script>
@endsection()