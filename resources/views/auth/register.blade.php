@extends('layouts.app')

@section('custom_style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/register.css') }}">
@endsection
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container" id="register">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light float-right">
          <button class="navbar-toggler" type="button" data-toggle="collapse" 
            data-target="#navbar-element" aria-controls="navbar-element" 
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar-element">
            <div class="navbar-nav">
              <a class="nav-item nav-link login-btn" href="{{ url('/') }}">LOGIN</a>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="row">
      <div id="register-section" class="col-md-6 col-centered">
        <div class="logo">
          <img class="col-5 col-centered" src="{{ asset('images/blue.png') }}">
        </div>
        <register></register>
      </div>
    </div>
  </div>
@section('custom_script')
  <script type="text/javascript" src="{{ asset('js/register.js') }}"></script>
@endsection
