@extends('layouts.app')

@section('custom_style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/resetpassword.css') }}">
@endsection

  <meta charset="utf-8" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container" id="resetpassword-page">
    <div class="row">
      <div class="col-md-12">
        <div class="row" id="resetpassword">
          <div id="resetpassword-section" class="col-md-6 col-centered">
            <div class="logo">
              <img class="col-5 col-centered" src="{{ asset('images/blue.png') }}">
            </div>
            @if ($errors->any())
              <div class="alert alert-danger alert-dismissible">
                @foreach ($errors->all() as $error)
                  <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            @endif
            <form novalidate id="resetpassword-form" method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
              @csrf
              <input type="hidden" name="token" value="{{ $token }}">
              <div class="form-group">
                <div class="col-md-12">
                  <input type="hidden" id="email" name="email" value="{{ $email ?? old('email') }}">
                </div>
              </div>
              <resetpassword></resetpassword>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

@section('custom_script')
  <script type="text/javascript" src="{{ asset('js/resetpassword.js') }}"></script>
@endsection()