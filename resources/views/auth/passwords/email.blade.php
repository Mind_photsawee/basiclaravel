@extends('layouts.app')

@section('custom_style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/forgetpassword.css') }}">
@endsection

  <meta charset="utf-8" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container" id="forgetpassword-page">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light float-right">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-element" aria-controls="navbar-element" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar-element">
            <div class="navbar-nav">
              <a class="nav-item nav-link signup-btn" href="{{ url('login') }}">LOGIN</a>
            </div>
          </div>
        </nav>
      </div>
    </div>
    <div class="row" id="forgetpassword">
      <div id="forgetpassword-section" class="col-md-6 col-centered">
        <div class="logo">
          <img class="col-5 col-centered" src="{{ asset('images/blue.png') }}">
        </div>
        <div class="card-body">
          @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
          @endif
          @if ($errors->any())
            <div class="alert alert-danger alert-dismissible">
              @foreach ($errors->all() as $error)
                <p>{{ $error }}</p>
              @endforeach
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          <form novalidate id="forgetpassword-form" method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
            @csrf
            <div class="form-group">
              <div class="col-md-12">
                <forgetpassword></forgetpassword>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@section('custom_script')
  <script type="text/javascript" src="{{ asset('js/forgetpassword.js') }}"></script>
@endsection()
