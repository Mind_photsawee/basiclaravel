@extends('layouts.app')

@section('custom_style')
  <link rel="stylesheet" type="text/css" href="{{ asset('css/login-page.css') }}">
@endsection
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="container" id="login-page">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-expand-lg navbar-light float-right">
          <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbar-element" aria-controls="navbar-element" 
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbar-element">
            <div class="navbar-nav">
              <a class="nav-item nav-link" href="javascript:void(0);">NOT A  MEMBER ?</a>
              <a class="nav-item nav-link signup-btn" href="{{ url('register') }}">REGISTER</a>
            </div>
          </div>
        </nav>
      </div>
    </div>
  <div class="row" id="login">
      <div id="login-section" class="col-md-6 col-centered">
        <div class="logo">
          <img class="col-5 col-centered" src="{{ asset('images/blue.png') }}">
        </div>
        @if (session('msg_success'))
          <div class="alert alert-success">
            {{ session('msg_success') }}
          </div>
        @elseif (session('msg_error'))
          <div class="alert alert-danger">
            {{ session('msg_error') }}
          </div>
        @endif
        <login></login>
      </div>
    </div>
    <p class="text-center"><a href="{{ url('password/reset') }}">FORGET PASSWORD ?</a></p>
  </div>
@section('custom_script')
  <script type="text/javascript" src="{{ asset('js/login.js') }}"></script>
@endsection()
