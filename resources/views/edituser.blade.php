@extends('layouts.app')
  
@section('custom_style')  
  <!-- Deashboard -->
    <!-- Bootstrap Core CSS -->
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
  <!-- End Deashboard -->
@endsection

@section('content')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <div class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
      <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
      </svg>
    </div>
    <div id="edit-user">
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
      <edituser></edituser>
    </div>
  </div>
@endsection

@section('custom_script')
  <script type="text/javascript" src="{{ asset('js/edituser.js') }}"></script>
@endsection()