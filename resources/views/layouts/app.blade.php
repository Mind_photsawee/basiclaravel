<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
    <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('custom_style')

  </head>
  <body>
    <div id="app">
      @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <!-- Deashboard -->
      <!-- All Jquery -->
      <script src="{{ asset('js/lib/jquery/jquery.min.js') }}" defer></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{ asset('js/lib/bootstrap/js/popper.min.js') }}" defer></script>
      <script src="{{ asset('js/lib/bootstrap/js/bootstrap.min.js') }}" defer></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{ asset('js/jquery.slimscroll.js') }}" defer></script>
      <!--Menu sidebar -->
      <script src="{{ asset('js/sidebarmenu.js') }}" defer></script>
      <!--stickey kit -->
      <script src="{{ asset('js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}" defer></script>
      <!--Custom JavaScript -->
      <script src="{{ asset('js/custom.min.js') }}" defer></script>
    <!-- End Deashboard -->
    
    @yield('custom_script')

  </body>
</html>
