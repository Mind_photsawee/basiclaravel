<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title> @yield('title') </title>
  
  @yield('custom_style')

</head>
  <body>
    
    <div>
    
      Photsawee Khonchoho
      @yield('content')
      
    </div>

    @yield('custom_script')

  </body>
</html>