window.Vue = require('vue');

Vue.component('dashboard', require('./components/dashboard/main.vue'));

const dashboard = new Vue({
  el: '#dashboard-section'
});