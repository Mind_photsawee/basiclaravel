window.Vue = require('vue');

Vue.component('profile', require('./components/dashboard/profile.vue'));

const profile = new Vue({
  el: '#profile-section'
});