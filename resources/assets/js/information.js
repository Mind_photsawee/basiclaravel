window.Vue = require('vue');

Vue.component('information', require('./components/dashboard/information.vue'));

const information = new Vue({
  el: '#information-section'
});