<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1', 'middleware' => 'json'], function() {
  Route::post('login', 'Api\LoginController@login');
  Route::post('register', 'Api\RegisterController@register');
  Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user', 'Admin\UserController@test');
    Route::get('user/{email}', 'Admin\UserController@showUser');
    Route::get('server', 'Admin\UserController@server');
    Route::get('adminShowdata/{id}', 'Admin\UserController@showData');
    Route::put('adminUpdate/{id}', 'Admin\UserController@update');
    Route::get('userShowprofile/{email}', 'Api\UserController@showProfile');
    Route::put('userUpdate/{email}', 'Api\UserController@update');
    Route::get('sizeServer', 'ServerController@sizeServer');
    Route::get('vmid','ServerController@genVmid');
    Route::get('user_id/{email}','ServerController@FK_server');
  });
});
