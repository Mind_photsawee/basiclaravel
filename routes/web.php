<?php

use App\User;
use App\Http\Resources\User as UserResource;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// อันนี้พี่อธิบายไปแล้ว พอจำได้นะ
Route::get('/test', function () {
  return 'Hello World';
});
// เรียกใช้ fuction main ของ controller ที่ชื่อว่า BasicController ที่อยู่ใน folder basicLaravel
Route::get('/testController', 'basicLaravel\BasicController@main');
/*
  ส่งค่าไปยัง controller เพียงค่าเดียว และต้องมีค่าส่งไป ไม่มีไม่ได้
  เช่นว่า localhost:8000/testController/...... ตรงที่ ..... เราก็พิมคำที่เราจะส่งไปได้เลย
*/
Route::get('/testController/{parameter}', 'basicLaravel\BasicController@passingData');
// 2 ตัว
// Route::get('/testController/{parameter}/{parameter2}', 'basicLaravel\BasicController@passingData2');
// 3 ตัว
// Route::get('/testController/{parameter}/{parameter2}/{parameter3}', 'basicLaravel\BasicController@passingData3');
// มีค่า หรือ ไม่มีค่าที่จะส่งก็ได้
Route::get('/param/{passing?}', 'basicLaravel\BasicController@passingParam');