let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/register.js', 'public/js')
    .js('resources/assets/js/login.js', 'public/js')
    .js('resources/assets/js/forgetpassword.js', 'public/js')
    .js('resources/assets/js/resetpassword.js', 'public/js')
    .js('resources/assets/js/dashboard.js', 'public/js')
    .js('resources/assets/js/server.js', 'public/js')
    .js('resources/assets/js/orderServer.js', 'public/js')
    .js('resources/assets/js/information.js', 'public/js')
    .js('resources/assets/js/sshkey.js', 'public/js')
    .js('resources/assets/js/dnskey.js', 'public/js')
    .js('resources/assets/js/sidebarheader.js', 'public/js')
    .js('resources/assets/js/profile.js', 'public/js')
    .js('resources/assets/js/listuser.js', 'public/js')
    .js('resources/assets/js/edituser.js', 'public/js')
    .sass('resources/assets/sass/login-page.scss', 'public/css')
    .sass('resources/assets/sass/resetpassword.scss', 'public/css')
    .sass('resources/assets/sass/forgetpassword.scss', 'public/css')
    .sass('resources/assets/sass/home.scss', 'public/css')
    .sass('resources/assets/sass/role-index.scss', 'public/css')
    .sass('resources/assets/sass/register.scss', 'public/css');
