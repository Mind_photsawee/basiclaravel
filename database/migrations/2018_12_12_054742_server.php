<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Server extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('server', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('vmid');
      $table->string('virtio1');
      $table->string('cores');
      $table->string('bwlimit');
      $table->string('memory');
      $table->string('name');
      $table->string('nameserver');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('server');
  }
}
