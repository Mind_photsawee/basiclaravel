<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInServerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('server', function (Blueprint $table) {
          $table->dropColumn('virtio1');
          $table->string('rootfs')->nullable();
          $table->string('root_password')->nullable();
          $table->string('ip')->nullable();
          $table->unsignedInteger('dns_id')->nullable();
          $table->foreign('dns_id')->references('id')->on('dns');
          $table->unsignedInteger('sshkey_id')->nullable();
          $table->foreign('sshkey_id')->references('id')->on('sshkey');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
    }
}
