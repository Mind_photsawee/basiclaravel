<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SizeServer extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('size_server', function (Blueprint $table) {
      $table->increments('id');
      $table->string('memory');
      $table->string('cpu');
      $table->string('ssd');
      $table->string('bandwidth');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('size_server');
  }
}
