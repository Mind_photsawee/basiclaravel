<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfile extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->string('first_name')->nullable();
      $table->string('last_name')->nullable();
      $table->string('phone_number')->nullable();
      $table->string('address')->nullable();
      $table->string('address_line2')->nullable();
      $table->string('city')->nullable();
      $table->string('zip_code')->nullable();
      $table->string('country')->nullable();
      $table->string('company')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('users', function (Blueprint $table) {
      $table->dropColumn('first_name');
      $table->dropColumn('last_name');
      $table->dropColumn('phone_number');
      $table->dropColumn('address');
      $table->dropColumn('address_line2');
      $table->dropColumn('city');
      $table->dropColumn('zip_code');
      $table->dropColumn('country');
      $table->dropColumn('company');
    });
  }
}
